function perfectCity(dep, dest){
    let dy = Math.abs(dep[1]-dest[1]);
    let dx = Math.abs(dep[0]-dest[0]);
    let route1,
        route2;

        if(Number.isInteger(dep[0]) && Number.isInteger(dest[0])){
            route1 = dx+dy;
            route2 = (Math.ceil(dep[0])-dep[1]) + dx + (Math.ceil(dep[1])-dep[1]);
        } else if (Number.isInteger(dep[1]) && Number.isInteger(dest[1])){
            route1 = (Math.ceil(dep[0])-dep[0]) + dy + (Math.ceil(dest[0])-dest[0]);
            route2 = (dep[0]-Math.floor(dep[0])) + dy + dest[0]-Math.floor(dep[0]);
        }

            console.log(`Route 1 is ${route1}`);
            console.log(`Route 2 is ${route2}`);

            if(route1<route2){
                console.log(`The shortest way is route1 - ${route1}` );
            } else {
                console.log(`The shortest way is route2 - ${route2}` );
            }
  };
//perfectCity([1.3,2],[2.5,1]);
perfectCity([0.4,1],[0.9,3]);
//perfectCity([3,3.5],[4,2.5]);
//perfectCity([1,3.7],[2,2.2]);
//perfectCity([0.6,1],[0.9,2]);
